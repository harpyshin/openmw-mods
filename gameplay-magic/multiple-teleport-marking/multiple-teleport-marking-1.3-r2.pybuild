# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import File, InstallDir, Pybuild1
from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "Multiple Teleport Marking"
    DESC = "Replaces the built-in mark & recall spells to allow marking up to 12 places"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/44825"
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    NEXUS_URL = HOMEPAGE
    # Note: TR version is incompatible with TR Preview
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        shotn? (
            landmasses/skyrim-home-of-the-nords
        )
        tr? (
            landmasses/tamriel-data
            landmasses/tamriel-rebuilt[-preview]
        )
        province-cyrodiil? (
            landmasses/province-cyrodiil
        )
        abandoned-flat? (
            arch-player/abandoned-flat
        )
        uviriths-legacy? (
            quests-factions/uviriths-legacy
        )
    """
    IUSE = "minimal levelled tr abandoned-flat shotn province-cyrodiil uviriths-legacy"
    REQUIRED_USE = "?? ( minimal tr )"
    SRC_URI = """
        !minimal? (
            Multiple_Teleport_Marking_Mod_(OpenMW_and_provinces)-44825-1-3-1552676304.zip
        )
        minimal? (
            Multiple_Teleport_Marking_Mod_(OpenMW_vanilla)-44825-1-1552676488.zip
        )
        levelled? (
            MultiMark_-_Mysticism_Balance-44825-1-1.zip
        )
        province-cyrodiil? (
            Province_Cyrodiil_Stirk_August_2018_update-44825-1808.zip
        )
        tr? (
            Multiple_Teleport_Marking_Mod_(OpenMW_and_TR_and_provinces)_TR_2101_update-44825-1-3-TR2101-1618780805.zip
        )
        shotn? (
            Skyrim_HotN_January_2020_Plugin-44825-2101-1618778539.zip
        )
        abandoned-flat? (
            https://modding-openmw.com/files/MultipleTeleportMarkingOpenMW-AbandonedFlat.zip
        )
        uviriths-legacy? ( Uvirith's_Legacy_plugin-44825-1.zip )
    """
    # MultiMark_Companion_Teleport_compatibility-44825-1.zip

    # Only one of these should be enabled, and optional plugins should load after them
    MAIN_PLUGINS = [
        "MultiMarkOMW-1.3.esp",
        "MultiMarkOMW-1.3-TR1912.esp",
        "MultiMarkOMW.esp",
    ]
    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[File("MultiMarkOMW-1.3.esp")],
            S=(
                "Multiple_Teleport_Marking_Mod_(OpenMW_and_provinces)"
                "-44825-1-3-1552676304"
            ),
            REQUIRED_USE="!minimal !tr",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("MultiMarkOMW.esp")],
            S="Multiple_Teleport_Marking_Mod_(OpenMW_vanilla)-44825-1-1552676488",
            REQUIRED_USE="minimal",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("MultiMarkOMW-1.3-TR2101.esp")],
            S=(
                "Multiple_Teleport_Marking_Mod_(OpenMW_and_TR_and_provinces)"
                "_TR_2101_update-44825-1-3-TR2101-1618780805"
            ),
            REQUIRED_USE="tr",
        ),
        InstallDir(
            ".",
            PLUGINS=[
                File("MultiMarkOMW_StirkPlugin_2018-08.ESP", OVERRIDES=MAIN_PLUGINS)
            ],
            S="Province_Cyrodiil_Stirk_August_2018_update-44825-1808",
            REQUIRED_USE="province-cyrodiil",
        ),
        InstallDir(
            ".",
            PLUGINS=[
                File(
                    "MultiMarkOMW_SkyPlugin2021_01_11_(v21.01a).ESP",
                    OVERRIDES=MAIN_PLUGINS,
                )
            ],
            S="Skyrim_HotN_January_2020_Plugin-44825-2101-1618778539",
            REQUIRED_USE="shotn",
        ),
        InstallDir(
            ".",
            PLUGINS=[
                File("MultiMarkOMW-MysticismBalance-1.1.esp", OVERRIDES=MAIN_PLUGINS)
            ],
            S="MultiMark_-_Mysticism_Balance-44825-1-1",
            REQUIRED_USE="levelled",
        ),
        InstallDir(
            "MultipleTeleportMarkingOpenMW-AbandonedFlat",
            PLUGINS=[
                File(
                    "MultiMarkOMW_AbandonedFlat_Plugin.omwaddon", OVERRIDES=MAIN_PLUGINS
                )
            ],
            S="MultipleTeleportMarkingOpenMW-AbandonedFlat",
            REQUIRED_USE="abandoned-flat",
        ),
        # Patch for companion teleportation mod
        # InstallDir(
        #     ".",
        #     PLUGINS=[File("MultiMarkOMW-CompanionTeleportationPatch.esp")],
        #     S="MultiMark_Companion_Teleport_compatibility-44825-1",
        # ),
        InstallDir(
            ".",
            PLUGINS=[File("MultiMarkOMW_UL3.51Plugin.ESP", OVERRIDES=MAIN_PLUGINS)],
            S="Uvirith's_Legacy_plugin-44825-1",
            REQUIRED_USE="uviriths-legacy",
        ),
    ]
