# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import Pybuild1, InstallDir, File
import shutil
import os


class Package(Pybuild1):
    NAME = "Wizards Islands 1.1 RU"
    DESC = (
        "Giant plugin which adds new island bigger then Solstheim"
        "with up to 30 hours of additional gameplay"
    )
    HOMEPAGE = "https://tesall.ru/files/file/2210-wizards-isles/"
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    KEYWORDS = "openmw"
    TIER = 0
    IUSE = "l10n_ru +music"
    SRC_URI = """
        Data_Files.part01.rar
        Data_Files.part02.rar
        Data_Files.part03.rar
        Data_Files.part04.rar
        Data_Files.part05.rar
        Data_Files.part06.rar
        Data_Files.part07.rar
        l10n_ru? ( rusificator.7z )
        !music? ( WI_NonMusic.esp )
    """
    INSTALL_DIRS = [
        InstallDir(
            "Data_Files.part01",
            PLUGINS=[File("Wizards Islands - Scourge of the Frost Bringer.esm")],
            ARCHIVES=[File("Wizards_Islands.bsa")],
            S=".",
        ),
        InstallDir(
            "rusificator",
            REQUIRED_USE="l10n_ru",
            PLUGINS=[
                File("Wizards_Islands_Patch_v1.1.esp"),
                File("WI_NonMusic.esp", REQUIRED_USE="!music"),
            ],
            S=".",
        ),
    ]

    def src_unpack(self):
        self.unpack([self.A[0]])
        if "l10n_ru" in self.USE:
            self.unpack([self.A[7]])

        if "music" not in self.USE:
            esp = self.A[8]
            shutil.copy(esp.path, os.path.join(self.WORKDIR, "rusificator/", esp.name))

    def pkg_nofetch(self):
        print("Please download the following files from the url at the bottom")
        print("before continuing and move them to the download directory:")
        print(f"  {self.DOWNLOAD_DIR}")
        print()
        for source in self.UNFETCHED:
            print(f"  {source}")
        print()
        print(f" {self.HOMEPAGE}")
        print()
