# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import File, InstallDir, Pybuild1

from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "Get Wrecked - Shipwreck Overhaul"
    DESC = "Overhauls all shipwreck interiors with additional detail and individuality."
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/51237"
    KEYWORDS = "openmw"
    LICENSE = "attribution-distribution-nc"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        >=base/oaab-data-1.12.0
        >=landmasses/tamriel-data-8
        bcom? ( rr-better-ships-and-boats? ( ~arch-towns/beautiful-cities-of-morrowind-2.5.2 ) )
        of-pillows-and-peril? ( ~quests-misc/of-pillows-and-peril-1.0 )
        rr-better-ships-and-boats? ( ~assets-meshes/rr-better-ships-and-boats-1.8 )
        uncharted-artifacts? ( ~items-misc/uncharted-artifacts-1.0.4 )
    """
    IUSE = "bcom of-pillows-and-peril rr-better-ships-and-boats uncharted-artifacts"
    SRC_URI = """
        Get_Wrecked_1.1-51237-1-1-1652642235.rar
        bcom? ( rr-better-ships-and-boats? ( BCoM_and_RR_Better_Ships_and_Boats_Compatibility_Patch-51237-1-0-1652616517.rar ) )
        of-pillows-and-peril? ( Of_Pillows_and_Peril_Compatible_Version-51237-1-0-1652695100.rar )
        rr-better-ships-and-boats? ( RR_Better_Ships_and_Boats_Compatibility_Patch-51237-1-0-1652642389.rar )
        uncharted-artifacts? ( Uncharted_Artifacts_Compatibility_Patch-51237-1-0-1652620838.rar )
    """
    INSTALL_DIRS = [
        InstallDir(
            "Get Wrecked",
            PLUGINS=[File("GetWrecked.ESP")],
            S="Get_Wrecked_1.1-51237-1-1-1652642235",
        ),
        InstallDir(
            ".",
            DATA_OVERRIDES=[
                "arch-towns/beautiful-cities-of-morrowind",
                "assets-meshes/rr-better-ships-and-boats",
            ],
            PLUGINS=[File("RR_Better_Ships_n_Boats_Eng.esp")],
            S="BCoM_and_RR_Better_Ships_and_Boats_Compatibility_Patch-51237-1-0-1652616517",
            REQUIRED_USE="bcom rr-better-ships-and-boats",
        ),
        InstallDir(
            ".",
            PLUGINS=[File("GetWrecked_oPaP.ESP", BLACKLIST="GetWrecked.ESP")],
            S="Of_Pillows_and_Peril_Compatible_Version-51237-1-0-1652695100",
            REQUIRED_USE="of-pillows-and-peril",
        ),
        InstallDir(
            ".",
            DATA_OVERRIDES="assets-meshes/rr-better-ships-and-boats",
            PLUGINS=[File("RR_Better_Ships_n_Boats_Eng.ESP")],
            S="RR_Better_Ships_and_Boats_Compatibility_Patch-51237-1-0-1652642389",
            REQUIRED_USE="!bcom rr-better-ships-and-boats",
        ),
        InstallDir(
            ".",
            DATA_OVERRIDES="items-misc/uncharted-artifacts",
            PLUGINS=[File("Uncharted Artifacts.esp", OVERRIDES="GetWrecked.ESP")],
            S="Uncharted_Artifacts_Compatibility_Patch-51237-1-0-1652620838",
            REQUIRED_USE="uncharted-artifacts",
        ),
    ]
