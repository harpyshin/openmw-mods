# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import File, InstallDir, Pybuild1
from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "Total Arktwend Overhaul"
    DESC = "An overhaul for the English version of Arktwend"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/28293"
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/28293?tab=files&file_id=90512 -> TAO_-_The_Arktwend_Overhaul_Version_0_15-28293-0-15.7z
        https://www.nexusmods.com/morrowind/mods/45611?tab=files&file_id=1000010120 -> TAO_compatibility_patch-45611-0-1.zip
    """
    RDEPEND = "base/arktwend"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            S="TAO_-_The_Arktwend_Overhaul_Version_0_15-28293-0-15",
        ),
        InstallDir(
            "TAO",
            PLUGINS=[File("TAO.esp")],
            BLACKLIST=["Optional"],
            S="TAO_compatibility_patch-45611-0-1",
        ),
    ]

    def pkg_nofetch(self):
        super().pkg_nofetch()
        if self.UNFETCHED:
            print("Alternatively, you can also download the main file from:")
            print(
                "https://www.moddb.com/mods/arktwend-the-forgotten-realm/downloads/tao-the-arktwend-overhaul-version-015"
            )
